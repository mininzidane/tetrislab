
/**
 * Return url from project root from relative url from public folder
 *
 * @param {string} relativePath
 * @returns {string}
 */
var getPathFromPublic = function (relativePath) {
	return 'public/' + relativePath;
};

/**
 * vendor js and css files, define type of file by its extension
 *
 * @type {object}
 */
var vendors = {
	/*'jquery': {
	 path: 'protected/vendor/bower/jquery/dist/',
	 files: ['jquery.js']
	 },
	 'bootstrap': {
	 path: 'protected/vendor/bower/bootstrap/dist/css/',
	 files: ['bootstrap.css']
	 }*/
};

/**
 * Get vendor files array by file extension
 *
 * @param {string} fileExtension
 * @returns {Array}
 */
var getVendorPaths = function (fileExtension) {
	var files = [];

	for (var paths in vendors) {
		if (!vendors.hasOwnProperty(paths)) {
			continue;
		}

		if (vendors[paths].files) {
			for (var i = 0; i < vendors[paths].files.length; i++) {
				// search for ending with '.{3}' (total 4 symbols)
				if (vendors[paths].files[i].indexOf('.' + fileExtension, vendors[paths].files[i].length - 4) == -1) {
					continue;
				}

				files.push(vendors[paths].path + vendors[paths].files[i]);
			}
		}
	}
	console.log('vendors ' + fileExtension + ': ' + files);
	return files;
};

// -- init plugins
var gulp = require('gulp');
var compass = require('gulp-compass');
var concat = require('gulp-concat');
var minify = require('gulp-clean-css');
var uglify = require('gulp-uglify');
var watch = require('gulp-watch');
var del = require('del');
// var gCallback = require('gulp-callback');
var babel = require('gulp-babel');
// var rename = require('gulp-rename');

var fs = require("fs");
var browserify = require("browserify");
var babelify = require("babelify");
// -- -- -- --

// -- init tasks into functions to run them without duplication
var tasks = {};
// -- Unneeded, for example only
tasks.es15 = function () {
	return gulp.src(getPathFromPublic('js/src/*.js'))
		.pipe(babel({
			'presets': ['es2015'],
			'plugins': ['transform-react-jsx']
		}))
		.pipe(gulp.dest(getPathFromPublic('js')));
};
// -- -- -- --

tasks.sass = function () {
	// clear compiled sass files before compiling because gulp-compass does not update existing files
	del([getPathFromPublic('css/sass/compiled/*')], function (err, deletedFiles) {
		console.log('Files deleted:', deletedFiles.join(', '));
	});

	console.log('Run SASS');
	return gulp
		.src(getPathFromPublic('css/sass/main.scss'))
		.pipe(compass({
			project: __dirname,
			//config_file: getPathFromPublic('config.rb'), // it could be unnecessary
			image: getPathFromPublic('images'),
			css: getPathFromPublic('css'),
			//sourcemap: true, // todo add concatted source map support
			sass: getPathFromPublic('css/sass')
		}));
};

tasks.css = function () {
	return tasks.sass().pipe(concat('main.css'), {newLine: '\n/* ============================ */\n\n'});
};

tasks.js = function () {
	tasks.browserify();
	console.log('Run JS concat');
	return gulp
		.src(getVendorPaths('js').concat([
			// add your js here: use getPathFromPublic() to get relative path from public folder
			getPathFromPublic('js/index.js')
		]))
		.pipe(concat('index.js', {newLine: '\n/* ============================ */\n\n'}))
};

tasks.browserify = function () {
	console.log('Run browserify');
	return browserify({debug: true})
		.transform(babelify, {presets: ['es2015'], plugins: ['transform-react-jsx']})
		.require(getPathFromPublic('js/src/index.js'), {entry: true})
		.bundle()
		.on("error", function (err) {
			console.log("Error: " + err.message);
		})
		.pipe(fs.createWriteStream(getPathFromPublic('js/index.js')));
};
// -- -- -- --

// sass/compass compiler (use 'gulp sass')
gulp.task('sass', function () {
	tasks.sass();
});

// concat sass compiled css with vendor files
gulp.task('css', function () {
	console.log(getVendorPaths('css'));
	tasks.css().pipe(gulp.dest(getPathFromPublic('css')));
});

// task for preparing js
gulp.task('js', function () {
	tasks.js().pipe(gulp.dest(getPathFromPublic('js')));
});

gulp.task('browserify', function () {
	tasks.browserify();
});

// dev gulp task (by default) without js and css minifies
gulp.task('default', function () {
	tasks.css().pipe(gulp.dest(getPathFromPublic('css')));
	tasks.js().pipe(gulp.dest(getPathFromPublic('js')));
});

// watcher for default task
gulp.task('watch', function () {
	return watch([
			'gulpfile.js',
			getPathFromPublic('css/sass/**/*.scss'),
			getPathFromPublic('js/src/*.js')
		],
		function () {
			tasks.css().pipe(gulp.dest(getPathFromPublic('css')));
			tasks.js().pipe(gulp.dest(getPathFromPublic('js')));
		});
});

// production gulp task with minifies
gulp.task('uglify', function () {
	gulp.src(getPathFromPublic('css/main.css'))
		.pipe(minify())
		.pipe(gulp.dest(getPathFromPublic('css')));

	gulp.src(getPathFromPublic('js/index.js'))
			.pipe(uglify())
			.pipe(gulp.dest(getPathFromPublic('js')));
});