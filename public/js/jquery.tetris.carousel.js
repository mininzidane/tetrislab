/**
 * Created by JetBrains PhpStorm.
 * User: Lukin Anton
 * Date: 12.09.12
 * Time: 21:38
 */
(function($) {
    $.fn.tetris = function(opts) {
        var defaults = {
                fallingDownSpeed: 200,
                fadeOutSpeed: 200,
                cubeWidth: 90,
                cubeHeight: 90,
                rowCount: 7,
                colCount: 7,
                buttonPrevHTML: "<div></div>",
                buttonNextHTML: "<div></div>",
                onAfterPrevFrame: function() {
                },
                onAfterNextFrame: function() {
                }
            },
            opts = $.extend(defaults, opts);
        return this.each(function() {
            var $that = $(this)
                    .after($(opts.buttonPrevHTML).addClass("tetris-prev"))
                    .after($(opts.buttonNextHTML).addClass("tetris-next")),
                $parent = $that.parent(),
                $newFrame, cubeWidth = opts.cubeWidth,
                cubeHeight = opts.cubeHeight,
                rowCount = opts.rowCount,
                colCount = opts.colCount,
                $currentFrame = $that.children("li:first");
            $that.children("li:gt(0)").hide();

            var randomize = function(max) {
                    max = max || rowCount - 1;
                    return Math.round(Math.random() * max);
                },
                buildTetris = function($frame, rows, cols) {
                    rows = rows || rowCount;
                    cols = cols || colCount;
                    var rndm = 0, $newCube, y = -cubeHeight;
                    for (var i = 0; i < rows; i++) {
                        y += cubeHeight;
                        for (var k = 0; k < cols; k++) {
                            var x = 0;
                            rndm = randomize(7);
                            cubes = "";
                            x = k * cubeWidth;
                            for (var j = 0; j < 4; j++) {
                                cubes += "<div class='cube cube-" + rndm + "'></div>";
                            }
                            $newCube = $("<div class='cube-wrapper'></div>")
                                .css({
                                    left: x,
                                    top: y
                                })
                                .attr({
                                    row: i,
                                    col: k
                                }).addClass("row" + i + " col" + k).append(cubes);
                            $(".tetris-int-link", $frame).append($newCube);
                        }
                    }
                },
                rebuildRowIndexes = function($objs) {
                    return $objs.each(function() {
                        var row = $(this).attr("row"),
                            newRow = parseInt(row) + 1;
                        $(this).removeClass("row" + row).addClass("row" + newRow).attr("row", newRow);
                    });
                },
                destroyCube = function(row, col, times, callback) {
                    if (typeof times == "undefined") {
                        times = 1;
                    }
                    callback = callback || function() {
                    };
                    var cssClass = ".row" + row + ".col" + col,
                        $cubes = $that.find(".cube-wrapper"),
                        $cube = $cubes.filter(cssClass),
                        fallingDownSpeed = opts.fallingDownSpeed,
                        fadeOutSpeed = opts.fadeOutSpeed,
                        recursion = function(_times) {
                            _times = _times || times - 1;
                            destroyCube(randomize(), randomize(), _times, callback);
                        };
                    if (!$(cssClass).length) {
                        recursion(times);
                    } else return times > 0 ?
                        $cube.fadeOut(fadeOutSpeed, function() {
                            var $cubesAbove = $();
                            for (var i = 0; i < row; i++) {
                                $cubesAbove = $cubesAbove.add($cubes.filter(".row" + i + ".col" + col));
                            }
                            if ($cubesAbove.length > 0) {
                                $cubesAbove.not(":last").animate({
                                    top: "+=" + cubeHeight
                                }, fallingDownSpeed, function() {
                                    rebuildRowIndexes($(this));
                                });
                                $cubesAbove.last().animate({
                                    top: "+=" + cubeHeight
                                }, fallingDownSpeed, function() {
                                    rebuildRowIndexes($(this));
                                    $cube.remove();
                                    recursion();
                                });
                            } else {
                                recursion();
                            }
                        }) : callback();
                },
                destroyRow = function(row, times, callback) {
                    if (typeof times == "undefined") {
                        times = 1;
                    }
                    callback = callback || function() {
                    };
                    var cssClass = ".row" + row,
                        $cubes = $that.find(".cube-wrapper"),
                        $row = $cubes.filter(cssClass),
                        fallingDownSpeed = opts.fallingDownSpeed,
                        fadeOutSpeed = opts.fadeOutSpeed,
                        recursion = function(_times) {
                            _times = _times || times - 1;
                            destroyRow(randomize(), _times, callback);
                        };
                    if (!$(cssClass).length) {
                        recursion(times);
                    } else {
                        if (times > 0) {
                            $row.not(":last").fadeOut(fadeOutSpeed);
                            $row.last().fadeOut(fadeOutSpeed, function() {
                                var $cubesAbove = $();
                                for (var i = 0; i < row; i++) {
                                    $cubesAbove = $cubesAbove.add($cubes.filter(".row" + i));
                                }
                                if ($cubesAbove.length > 0) {
                                    $cubesAbove.filter(":not(:last)").animate({
                                        top: "+=" + cubeHeight
                                    }, fallingDownSpeed, function() {
                                        rebuildRowIndexes($(this));
                                    });
                                    $cubesAbove.filter(":last").animate({
                                        top: "+=" + cubeHeight
                                    }, fallingDownSpeed, function() {
                                        rebuildRowIndexes($(this));
                                        $row.remove();
                                        recursion();
                                    });
                                } else {
                                    recursion();
                                }
                            });
                        } else {
                            callback();
                        }
                    }
                },
                clearTetris = function(callback) {
                    callback = callback || function() {
                    };
                    var $cubes = $currentFrame.find(".tetris-int-link > .cube-wrapper");
                    $cubes.filter(":visible:not(:last)").fadeOut(500);
                    $cubes.filter(":visible:last").fadeOut(500, function() {
                        $cubes.remove();
                        callback();
                    });
                },
                prevRound = function() {
                    $("li", $that).hide();
                    $newFrame.fadeIn(opts.speed, function() {
                        $currentFrame = $newFrame;
                        opts.onAfterPrevFrame($currentFrame, this);
                    });
                },
                nextRound = function() {
                    $("li", $that).hide();
                    $newFrame.fadeIn(opts.speed, function() {
                        $currentFrame = $newFrame;
                        opts.onAfterNextFrame($currentFrame, this);
                    });
                };

            $(".tetris-prev", $parent).click(function() {
                var $prev = $currentFrame.prev("li");
                $newFrame = $prev.length ? $prev : $("li:last", $that);

                // init tetris
                buildTetris($currentFrame);

                // init cube desctroy and falling down cubes above
                destroyCube(randomize(), randomize(), 2, function() {
                    destroyRow(randomize(), 2, function() {
                        destroyCube(randomize(), randomize(), 2, function() {
                            destroyRow(randomize(), 1, function() {
                                clearTetris(function() {
                                    prevRound();
                                });
                            });
                        });
                    });
                });
            });

            $(".tetris-next", $parent).click(function() {
                var $next = $currentFrame.next("li");
                $newFrame = $next.length ? $next : $("li:first", $that);

                // init tetris
                buildTetris($currentFrame);

                // init cube desctroy and falling down cubes above
                destroyCube(randomize(), randomize(), 2, function() {
                    destroyRow(randomize(), 2, function() {
                        destroyCube(randomize(), randomize(), 2, function() {
                            destroyRow(randomize(), 1, function() {
                                clearTetris(function() {
                                    nextRound();
                                });
                            });
                        });
                    });
                });
            });
        });
    };
})(jQuery);