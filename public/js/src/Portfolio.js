import React from 'react'
import PortfolioListItem from './PortfolioListItem'
import PortfolioItem from './PortfolioItem'

export default class Portfolio extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			route: window.location.hash.substr(1)
		};
		this.data = {};
		this.source = $('#portfolio-init').data('url');
		this.$body = $('body');
	}

	getJSONData(callback = function(){}) {
		this.serverRequest = $.getJSON(this.source, function (data) {
			this.data = data;
			this.setState(data);
			callback();
		}.bind(this))
	}

	componentDidMount() {
		window.addEventListener('hashchange', () => {
			this.setState({
				route: window.location.hash.substr(1)
			})
		});

		this.getJSONData();

		var that = this;
		this.$body.on('click', '.js-load-portfolio-item', function () {
			that.source = $(this).attr('data-item');
			that.getJSONData();
			return false;
		});

		this.$body.on('click', '.js-portfolio-link', function () {
			that.source = $(this).attr('href');
			that.getJSONData(() => {
				window.location.hash = '/item-' + that.data.id;
			});
			return false;
		});
	}

	componentWillUnmount() {
		this.serverRequest.abort();
	}

	render() {
		var Child,
			matches;

		Child = PortfolioListItem;
		// portfolio item view
		if (matches = this.state.route.match(/^\/item-(\d+)$/)) {
			Child = PortfolioItem;
			this.source = '/ajax/portfolio-item/?id=' + matches[1];
			this.getJSONData();
		}

		return (
			<Child data={this.data} />
		)
	}
}