import React from 'react'

export default class PortfolioListItem extends React.Component {

	render() {
		return (
			<div className="portfolio">
				<h2 className="portfolio__title">{this.props.data.title}</h2>
				<p>
					<a target="_blank" href={this.props.data.url}>{this.props.data.url}</a>
				</p>
				<a href={this.props.data.portfolioUrl} className="portfolio__link js-portfolio-link">
					<img className="portfolio__image portfolio__image_fixed-height" src={this.props.data.image} alt=""/>
				</a>
				<div className="portfolio__teaser">{this.props.data.teaser}</div>
				{this.props.data.prevItem? <div className="portfolio__controls js-load-portfolio-item" data-item={this.props.data.prevItem}></div>: ''}
				{this.props.data.nextItem? <div className="portfolio__controls portfolio__controls_next js-load-portfolio-item" data-item={this.props.data.nextItem}></div>: ''}
			</div>
		);
	}
}