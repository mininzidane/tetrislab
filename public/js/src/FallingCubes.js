export default class FallingCubes {

	constructor() {
		this.positionCount = 8;
		this.figures = [
			'<div class="figure figure-T"><div class="cube">' +
			'</div><div class="cube" style="left: 45px;">' +
			'</div><div class="cube" style="left: 90px;"></div>' +
			'<div class="cube" style="left: 45px; top: 0;"></div></div>',

			'<div class="figure figure-Z">' +
			'<div class="cube"></div><div class="cube" style="left: 45px;">' +
			'</div><div class="cube" style="left: 90px; top: 0;"></div>' +
			'<div class="cube" style="left: 45px; top: 0;"></div></div>',

			'<div class="figure figure-cube">' +
			'<div class="cube"></div>' +
			'<div class="cube" style="left: 0; top: 0;"></div>' +
			'<div class="cube" style="left: 45px; top: 0;"></div>' +
			'<div class="cube" style="left: 45px; top: 45px;"></div></div>',

			'<div class="figure figure-line">' +
			'<div class="cube"></div><div class="cube" style="left: -45px;">' +
			'</div><div class="cube" style="left: 45px;"></div>' +
			'<div class="cube" style="left: 90px;"></div></div>',

			'<div class="figure figure-L">' +
			'<div class="cube"></div><div class="cube" style="left: 45px;">' +
			'</div><div class="cube" style="left: 90px;">' +
			'</div><div class="cube" style="left: 0; top: 0;"></div></div>'
		];
	}

	static randomInInterval(max, min = 0) {
		return min + Math.floor(Math.random() * (max - min));
	}

	getRandomCube() {
		return this.figures[Math.floor(Math.random() * this.figures.length)];
	}

	initCubes() {
		var uniqueClassName = 'class' + (Math.round(Math.random() * 10000000)),
			left = this.constructor.randomInInterval(this.positionCount),
			$elem = $(this.getRandomCube());

		$elem.appendTo('.body').css({
			top: -135,
			left: (100 / this.positionCount * left) + '%'
		}).addClass(uniqueClassName + ' figure-' + this.constructor.randomInInterval(5))
			.children().addClass('cube-' + this.constructor.randomInInterval(7));

		setTimeout(function () {
			$('.' + uniqueClassName).remove();
		}, 10000);
	}

	run() {
		this.initCubes();
		setInterval(function () {
			this.initCubes();
		}.bind(this), 3500);
	}
}