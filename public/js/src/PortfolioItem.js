import React from 'react'

export default class PortfolioItem extends React.Component {

	render() {
		return (
			<div className="portfolio">
				<div className="portfolio__back">
					<a href="#">
						<span className="glyphicon glyphicon-arrow-up"/> К списку работ
					</a>
				</div>
				<h2 className="portfolio__title">{this.props.data.title}</h2>
				<img className="portfolio__image" src={this.props.data.image_full} alt=""/>
				<div className="portfolio__teaser">{this.props.data.teaser}<br/>{this.props.data.desc}</div>
				<p>
					<a target="_blank" href={this.props.data.url} className="portfolio__link">{this.props.data.url}</a>
				</p>
			</div>
		);
	}
}