import React from 'react'
import ReactDOM from 'react-dom'
import Portfolio from './Portfolio'
import FallingCubes from './FallingCubes'

(new FallingCubes()).run();

ReactDOM.render(
	<Portfolio/>,
	document.getElementById('content')
);