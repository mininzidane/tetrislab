<?php

$productionModeName = 'production';

defined('APPLICATION_MODE')	or define('APPLICATION_MODE',isset($_SERVER['APPLICATION_MODE'])? $_SERVER['APPLICATION_MODE']: $productionModeName);
defined('YII_DEBUG') or define('YII_DEBUG', APPLICATION_MODE != $productionModeName);

// -- mapping for YII predefined environment constants (values: prod, dev, test)
$env = APPLICATION_MODE;
if (APPLICATION_MODE == $productionModeName) {
	$env = 'prod';
}
defined('YII_ENV') or define('YII_ENV', $env);
// -- -- -- --

require(__DIR__ . '/../protected/vendor/autoload.php');
require(__DIR__ . '/../protected/vendor/yiisoft/yii2/Yii.php');

// calculate filename for config: use your own config file defined in environment variable APPLICATION_MODE
$configFileName = realpath(__DIR__ . implode(DIRECTORY_SEPARATOR, ['', '..', 'protected', 'config', 'env.php']));
$config = array_replace_recursive(
	require(__DIR__ . implode(DIRECTORY_SEPARATOR, ['', '..', 'protected', 'config', '']) . '_main.php'),
	require($configFileName)
);

(new yii\web\Application($config))->run();
