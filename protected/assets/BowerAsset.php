<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Asset bundle for the Bower installed js/css files.
 */
class BowerAsset extends AssetBundle {

	public $sourcePath = '@bower';
	public $js = [
//		'jquery/dist/jquery.min.js',
//		'bootstrap/dist/js/bootstrap.min.js',
	];
	public $css = [
		'bootstrap/dist/css/bootstrap.css',
	];
}
