<?php

return [
	'/'                       => 'main/index',
	'/<action>/'              => 'main/<action>', // common
	'/<controller>/<action>/' => '<controller>/<action>', // common
];
