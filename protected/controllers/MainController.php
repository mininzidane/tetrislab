<?php

namespace app\controllers;

use app\models\ContactForm;
use app\models\Portfolio;
use Yii;

class MainController extends BaseController {

	public function actions() {
		return [
			'error'   => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class'           => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST? 'testme': null,
			],
		];
	}

	public function actionIndex() {
		$portfolio = Portfolio::find()->one();
		return $this->render('index', [
			'portfolio' => $portfolio,
		]);
	}

	public function actionAbout() {
		$this->view->title = 'О веб-студии';
		return $this->render('about');
	}

	public function actionServices() {
		$this->view->title = 'Наши услуги';
		return $this->render('services');
	}

	public function actionContacts() {
		$this->view->title = 'Связь с нами';

		$model = new ContactForm();

		if (Yii::$app->request->isPost) {
			$model->load(Yii::$app->request->post());
			Yii::$app->session->setFlash('contacts', $model->sendAdminEmail());
		}

		return $this->render('contacts', [
			'model' => $model
		]);
	}
}
