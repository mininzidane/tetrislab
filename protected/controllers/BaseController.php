<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Page;
use yii\web\HttpException;

class BaseController extends Controller {

	public $enableCsrfValidation = false;
	public $layout = 'main';

	/**
	 * Makes array data to view as JSON
	 *
	 * @param array $data
	 * @return string
	 */
	public function renderJson(array $data) {
		header('Content-Type: application/json; charset=utf-8');
		return json_encode($data, defined('JSON_UNESCAPED_UNICODE')? JSON_UNESCAPED_UNICODE: 0);
	}

	protected function renderContentPage($url) {
		$page = Page::find()->byUrl($url)->one();

		if (!$page) {
			throw new HttpException(404, Yii::t('yii', 'Page not found.'));
		}
		return $this->render('page', [
			'page' => $page
		]);
	}
}
