<?php

namespace app\controllers;

use app\models\Portfolio;
use Yii;
use yii\helpers\Url;

class AjaxController extends BaseController {

	public function actionPortfolioItem($id) {
		$portfolio = Portfolio::findOne($id);
		$data = $portfolio->attributes;
		unset($data['active'], $data['created']);
		$data['portfolioUrl'] = Url::to(['ajax/portfolio-item', 'id' => $id]);

		$data['prevItem'] = $data['nextItem'] = null;
		if ($prev = $portfolio->getPrev()) {
			$data['prevItem'] = Url::to(['ajax/portfolio-item', 'id' => $prev->id]);
		}
		if ($next = $portfolio->getNext()) {
			$data['nextItem'] = Url::to(['ajax/portfolio-item', 'id' => $next->id]);
		}
		return $this->renderJson($data);
	}
}
