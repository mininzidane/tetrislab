<?php

use \app\models\Page;

class m160905_113155_create_page extends \yii\db\Migration {

	public function safeUp() {
		$this->createTable(Page::tableName(), [
			'id'      => $this->primaryKey(),
			'content' => 'TEXT NULL DEFAULT NULL',
			'url'     => 'VARCHAR(64) NOT NULL',
			'active'  => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 1',
			'created' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
		]);

		$content = [
			[
				'url'     => 'about',
				'content' => '<p>Tetris Lab&nbsp;— это&nbsp;команда молодых профессионалов в&nbsp;области веб-разработки. Мы&nbsp;не&nbsp;будем здесь описывать, что&nbsp;мы&nbsp;делаем сайты и&nbsp;мобильные приложения лучше всех, просто опишем условия работы с&nbsp;нами, которые может быть устроят потенциального заказчика.</p>
				<p>Этапы работы стандартные:</p>
				<ul>
				<li>получение информации о&nbsp;деятельности клиента</li>
				<li>проработка логики сайта</li>
				<li><a href="/services/">разработка</a> и&nbsp;утверждение дизайна сайта</li>
				<li>верстка и&nbsp;сборка сайта на&nbsp;CMS<span style="margin-right: 0.3em"> </span> <span style="margin-left: -0.3em">(систему</span> управления контентом)</li>
				</ul>
				<p>Основные приоритеты нашей команды&nbsp;— полная прозрачность работы над&nbsp;проектами для&nbsp;заказчика и&nbsp;управляемость рабочим процессом с&nbsp;его&nbsp;стороны</p>
				<p>В своей работе мы&nbsp;используем последние технологии веб-разработки, в&nbsp;связи с&nbsp;этим клиент должен понимать, что&nbsp;в&nbsp;зависимости от&nbsp;сложности дизайна могут иметь место различия в&nbsp;отображении сайта в&nbsp;различных браузерах. Несмотря на&nbsp;это, функционал сайта будет абсолютно кроссбраузерным</p>
				<div class="inner-cube1">&nbsp;</div>'
			],
			[
				'url'     => 'services',
				'content' => '<p>Мы продаем свои услуги по&nbsp;следующим направлениям:</p>
				<ul class="list-inline">
				<li>создание сайтов<span style="margin-right: 0.3em"> </span> <span style="margin-left: -0.3em">«под</span> ключ»</li>
				<li>продвижение сайтов в&nbsp;поисковых системах и&nbsp;размещение контекстной рекламы</li>
				<li>поддержка и&nbsp;администрирование существующих сайтов</li>
				</ul>
				<h2>Cоздание сайта<span style="margin-right: 0.3em"> </span> <span style="margin-left: -0.3em">«под</span> ключ»</h2>
				<p>В эту&nbsp;услугу входит создание дизайна будущего сайта, верстка и&nbsp;интеграция с&nbsp;CMS. Также, мы&nbsp;бесплатно поможем зарегистрировать на&nbsp;Ваше имя&nbsp;домен и&nbsp;хостинг. Стоимость работ полностью зависит от&nbsp;сложности сайта и&nbsp;высчитывается за&nbsp;каждый час&nbsp;работы.</p>
				<p>Мы являемся партнерами системы управления сайтами HostCMS. С&nbsp;информацией об&nbsp;этом продукте вы&nbsp;можете ознакомиться <a href="/services/on-hostcms/">здесь</a></p>
				<h2>Продвижение сайта</h2>
				<p>Под понятием<span style="margin-right: 0.3em"> </span> <span style="margin-left: -0.3em">«продвижения</span>» в&nbsp;поисковых системах мы&nbsp;подразумеваем увеличение посещаемости сайта с&nbsp;помощью переходов с<span style="margin-right: 0.3em"> </span> <span style="margin-left: -0.3em">«органической</span>» выдачи поисковых систем Яндекса и&nbsp;Гугла. В&nbsp;соответствии с&nbsp;реалиями текущих алгоритмов поисковиков, это&nbsp;работа довольно трудоемкая и&nbsp;требующая временных затрат<span style="margin-right: 0.3em"> </span> <span style="margin-left: -0.3em">(обычно</span> от&nbsp;полугода).</p>
				<p>Также возможен более быстрый способ попасть в&nbsp;топ&nbsp;выдачи&nbsp;— контекстная реклама: вы&nbsp;просто платите за&nbsp;переходы на&nbsp;Ваш&nbsp;сайт с&nbsp;выдачи по&nbsp;определенным запросам.</p>
				<p>Конечной целью обоих способов является увеличение продаж через сайт, как&nbsp;точку входа в&nbsp;Ваш&nbsp;бизнес</p>
				<h2>Поддержка сайтов</h2>
				<p>У вас&nbsp;есть рабочий сайт? Тогда мы&nbsp;готовы заниматься его&nbsp;развитием, поддержкой и&nbsp;вообще всем, что&nbsp;может понадобиться в&nbsp;ходе жизни любого сайта.</p>
				<p>Оставить заявку на&nbsp;любую из&nbsp;услуг Вы&nbsp;можете в&nbsp;нашей <a href="/contacts/">форме обратной связи</a>.</p>
				',
			],
		];
		foreach ($content as $value) {
			$this->insert(Page::tableName(), [
				'url'     => $value['url'],
				'content' => $value['content'],
			]);
		}
	}

	public function safeDown() {
		$this->dropTable(Page::tableName());
	}
}