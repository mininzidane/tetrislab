<?php

use yii\db\Migration;
use \app\models\Portfolio;

/**
 * Handles the creation for table `portfolio`.
 */
class m160907_130517_create_portfolio_table extends Migration {

	/**
	 * @inheritdoc
	 */
	public function up() {
		$this->createTable(Portfolio::tableName(), [
			'id'      => $this->primaryKey(),
			'title'   => 'VARCHAR(64) NOT NULL',
			'url'     => 'VARCHAR(64) NOT NULL',
			'image'   => 'VARCHAR(128) NULL DEFAULT NULL',
			'teaser'  => 'TEXT NULL DEFAULT NULL',
			'desc'    => 'TEXT NULL DEFAULT NULL',
			'active'  => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 1',
			'created' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
		]);
		$this->insert(Portfolio::tableName(), [
			'title' => 'Сайт ведущего Романа Ткаченко',
			'url' => 'http://romantkachenko.ru',
			'image' => '/images/portfolio/romantkachenko.png',
			'teaser' => 'Test teaser',
			'desc' => 'Test desc',
		]);
		$this->insert(Portfolio::tableName(), [
			'title' => 'Сайт мегаполиса',
			'url' => 'http://megapolis-video.ru',
			'image' => '/images/portfolio/megapolis-video.png',
			'teaser' => 'Test teaser 2',
			'desc' => 'Test desc 2',
		]);
	}

	/**
	 * @inheritdoc
	 */
	public function down() {
		$this->dropTable(Portfolio::tableName());
	}
}
