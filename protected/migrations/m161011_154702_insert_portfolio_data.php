<?php

use app\models\Portfolio;

class m161011_154702_insert_portfolio_data extends \yii\db\Migration {

	public function safeUp() {
		$table = Portfolio::tableName();
		$this->truncateTable($table);
		$content = [
			[
				'Производство видеороликов', // title
				'http://megapolis-video.ru', // url
				'megapolis-video.jpg', // teaser image
				'megapolis-video_full.png', // full image
				'Сайт компании, занимающейся производством корпоративных видеороликов и рекламы. Сайт на CMS Bitrix. Адаптивный дизайн на bootstrap.', // teaser
				'Встроенные ролики на HTML5, всплывающие окна, "карусели" для больших объемов информации' // desc
			],
			[
				'Ведущий Роман Ткаченко', // title
				'http://romantkachenko.ru', // url
				'romantkachenko.jpg', // teaser image
				'romantkachenko_full.png', // full image
				'Сайт-портфолио свадебного ведущего. Сайт на CMS Bitrix. Адаптивный дизайн на bootstrap.', // teaser
				'Парсер фотографий instagram, интерактивная всплывающая яндекс карта, плавный вертикальный скроллинг, всплывающие окна, ' // desc
			],
			[
				'Букеты и интерьер Доминик Декор', // title
				'http://studiodd.ru/', // url
				'studio-dd.jpg', // teaser image
				'studio-dd_full.png', // full image
				'Интернет-магазин букетов, предметов интерьера и подарков. Сайт на CMS Bitrix. Адаптивный дизайн на bootstrap.', // teaser
				'Модули: каталог товаров, поиск, личный кабинет, корзина, новости' // desc
			],
			[
				'Ароматические свечи Yankee Candle', // title
				'http://yc-aroma.ru', // url
				'yankeecandle.jpg', // teaser image
				'yankeecandle-full.png', // full image
				'Интернет-магазин ароматических свечей. Сайт на CMS Bitrix. Адаптивный дизайн на bootstrap.', // teaser
				'Модули: каталог товаров, поиск, личный кабинет, корзина, новости' // desc
			],
			[
				'IT решения Vektor Systems', // title
				'http://vektor.systems/', // url
				'vektor-systems.jpg', // teaser image
				'vektor-systems_full.png', // full image
				'Лэндинг IT компании. Одностраничный сайт. Адаптивный дизайн на bootstrap.', // teaser
				'Двухязычный лэндинг, всплывающие окна с меню и формами запроса, плавный скроллинг, ' // desc
			],
		];
		foreach ($content as $item) {
			$this->insert($table, [
				'title'      => $item[0],
				'url'        => $item[1],
				'image'      => "/images/portfolio/{$item[2]}",
				'image_full' => "/images/portfolio/{$item[3]}",
				'teaser'     => $item[4],
				'desc'       => $item[5],
			]);
		}
	}

	public function safeDown() {

	}
}
