<?php

use \app\models\Portfolio;

class m161003_151600_add_image_full extends \yii\db\Migration {

	public function safeUp() {
		$this->addColumn(Portfolio::tableName(), 'image_full', 'VARCHAR(128) NULL DEFAULT NULL AFTER image');
	}

	public function safeDown() {
		$this->dropColumn(Portfolio::tableName(), 'image_full');
	}
}
