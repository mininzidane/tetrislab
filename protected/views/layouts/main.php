<?php
use yii\helpers\Html;

use app\widgets\Nav;
use app\assets\AppAsset;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);

$action = Yii::$app->controller->action->id;
?>
<?php $this->beginPage() ?>
	<!DOCTYPE html>
	<html lang="<?= Yii::$app->language ?>">
	<head>
		<meta charset="<?= Yii::$app->charset ?>">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Разработка, создание и поддержка сайтов - веб студия Tetris Lab">
		<link rel="icon" type="image/x-icon" href="/images/favicon.jpg">
		<?= Html::csrfMetaTags() ?>
		<title><?= Html::encode($this->title) ?> - Tetris Lab</title>
		<?php $this->head() ?>
	</head>
	<body>
	<?php $this->beginBody() ?>

	<div class="body">
		<header>
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<a href="/" class="logo"></a>
					</div>
					<div class="col-sm-8">
						<?php
						$actionId = Yii::$app->controller->action->id;
						echo Nav::widget([
							'options' => ['class' => 'menu-nav'],
							'items'   => [
								[
									'label'  => 'О нас',
									'url'    => ['main/about'],
									'active' => $actionId == 'about',
								],
								[
									'label'  => 'Услуги',
									'url'    => ['main/services'],
									'active' => $actionId == 'services',
								],
								[
									'label'  => 'Контакты',
									'url'    => ['main/contacts'],
									'active' => $actionId == 'contacts',
								],
							]
						]) ?>
					</div>
				</div>
			</div>
		</header>
		<div class="container">
			<section class="content-wrapper">
				<h1 class="page-title"><?= $this->title ?></h1>
				<div class="content" id="content">
					<?= $content ?>
				</div>
			</section>
		</div>
		<footer>
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						&copy; Tetris Lab Studio <?= date('Y') ?>
					</div>
				</div>
			</div>
		</footer>
	</div>
	<?php $this->endBody() ?>
	</body>
	</html>
<?php $this->endPage() ?>