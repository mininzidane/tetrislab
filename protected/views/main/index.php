<?php
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var $this      yii\web\View
 * @var $portfolio \app\models\Portfolio
 */
$this->title = 'Разработка и создание сайтов для людей';
$this->registerJsFile('/js/index.js', ['depends' => ['yii\web\JqueryAsset', 'app\assets\BowerAsset']]);
?>
<div id="portfolio-init" data-url="<?= Url::to(['ajax/portfolio-item', 'id' => $portfolio->id]) ?>"></div>