<?php
use yii\helpers\Html;

/**
 * @var $this yii\web\View
 * @var $page \app\models\Page
 */

echo $page->content;