<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/**
 * @var $this  yii\web\View
 * @var $model \app\models\ContactForm
 * @var $form  yii\bootstrap\ActiveForm
 */
$flash = Yii::$app->session->getFlash('contacts');
?>

<?php /*<p>Телефон: <nobr>+7 (902) 528-62-98</nobr></p>*/ ?>

<?php if ($flash !== null) {
	echo Html::tag('div', $flash? 'Сообщение отправлено. Скоро мы свяжемся с вами': 'Ошибка', ['class' => 'alert alert-' . ($flash? 'success': 'danger')]);
}
?>
<?php $form = ActiveForm::begin([
	'id'          => 'contact-form',
	'fieldConfig' => [
		'template' => "<div class=\"row\"><div class=\"col-sm-4\">{label}</div>\n<div class=\"col-sm-8\">{input}{error}</div></div>",
	],
]); ?>
<?= $form->field($model, 'name') ?>
<?= $form->field($model, 'email') ?>
<?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>
<?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
	'captchaAction' => 'main/captcha',
	'template'      => '<div class="row"><div class="col-sm-4">{image}</div><div class="col-sm-8">{input}</div></div>',
]) ?>
<div class="form-group text-center">
	<?= Html::submitButton('Отправить', ['class' => 'btn btn-lg btn-primary', 'name' => 'contact-button']) ?>
</div>
<?php ActiveForm::end(); ?>

<div class="inner-cube2"></div>

