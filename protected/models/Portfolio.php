<?php

namespace app\models;

/**
 * @property int    $id
 * @property string $title
 * @property string $url
 * @property string $image
 * @property string $teaser
 * @property string $desc
 * @property bool   $active
 */
class Portfolio extends ActiveRecord {

	/**
	 * @return ActiveQuery
	 */
	public static function find() {
		return (new PortfolioQuery(get_called_class()))->active();
	}

	public function rules() {
		return [
			[['title', 'url'], 'required'],
			[['desc', 'teaser', 'active'], 'safe'],
		];
	}

	/**
	 * @return self
	 */
	public function getNext() {
		$next = $this->find()->where(['>', 'id', $this->id])->one();
		return $next;
	}

	/**
	 * @return self
	 */
	public function getPrev() {
		$prev = $this->find()->where(['<', 'id', $this->id])->orderBy('id DESC')->one();
		return $prev;
	}
}

class PortfolioQuery extends ActiveQuery {

	public function active() {
		$this->andWhere('active = 1');
		return $this;
	}
}