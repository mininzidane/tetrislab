<?php

namespace app\models;

use yii\base\Model;
use \Yii;

/**
 * @property int    $id
 * @property string $content
 * @property string $url
 * @property bool   $active
 */
class ContactForm extends Model {

	public $name;
	public $email;
	public $body;
	public $verifyCode;

	public function rules() {
		return [
			// name, email, subject and body are required
			[['name', 'email', 'body'], 'required'],
			// email has to be a valid email address
			['email', 'email'],
			// verifyCode needs to be entered correctly
			['verifyCode', 'captcha', 'captchaAction' => 'main/captcha'],
		];
	}

	/**
	 * @return array customized attribute labels
	 */
	public function attributeLabels() {
		return [
			'name'       => 'Ваше имя',
			'email'      => 'Электронный адрес',
			'body'       => 'Сообщение',
			'verifyCode' => 'Проверочный код',
		];
	}

	/**
	 * Sends an email to the specified email address using the information collected by this model.
	 * @return boolean whether the model passes validation
	 */
	public function sendAdminEmail() {
		if ($this->validate()) {
			return Yii::$app->mailer->compose()
				->setTo([Yii::$app->params['adminEmail'], Yii::$app->params['adminEmail2']])
				->setFrom([$this->email => $this->name])
				->setSubject('Запрос из формы обратной связи')
				->setTextBody($this->body)
				->send();
		} else {
			return false;
		}
	}
}