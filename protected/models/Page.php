<?php

namespace app\models;

/**
 * @property int    $id
 * @property string $content
 * @property string $url
 * @property bool   $active
 */
class Page extends ActiveRecord {

	/**
	 * @return PageQuery
	 */
	public static function find() {
		return (new PageQuery(get_called_class()))->active();
	}
}

class PageQuery extends ActiveQuery {

	public function active() {
		$this->andWhere('active = 1');
		return $this;
	}

	public function byUrl($url) {
		$this->andWhere('url = :url', [':url' => $url]);
		return $this;
	}
}